package com.myspring.myspring.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;



@RestController
public class Product {

    @Value("${server.port}")
    int port;

    @GetMapping("/product")
    public String product() {
        return "ports" + port;
    }
    

}
